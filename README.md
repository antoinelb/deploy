# Steps

1. Initialize server

- Change A record to new ip address
- `scp -r init root@antoinelb.ca:~`
- `ssh root@antoinelb.ca`
- `cd init`
- `sh init`
- `cd ..`
- `rm -r init`
- `exit`

2. Set up proxy

- `scp -r proxy db makefile server@antoinelb.ca:~`
- `ssh server@antoinelb.ca`
- `make update-domain domain=antoinelb.ca`
- `make init-proxy`
- `make up`

3. Set up new site

- add CNAME record
- `ssh -t server@antoinelb.ca 'export DB_PASS=xxxxx; $SHELL'`
- `make add-site site=auth port=8000 db_port=5432`
- `make restart-proxy`

4. (optional) Run server

- `scp env-vars server@antoinelb.ca:~/.env`
- `ssh server@antoinelb.ca`
- `make update-server token=xxxxxxx image=registry.gitlab.com/antoinelb/auth-api:latest site=auth`
