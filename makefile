API_KEY=$(shell cat keys/api-key)


up:
	@sudo podman pod create \
		--network host \
		--name pod
	@cd proxy && $(MAKE) up


down:
	@sudo podman pod stop pod
	@sudo podman pod rm pod


########
# init #
########

create-instance:
	@sh init/create-instance


init-proxy:
	@cd proxy && $(MAKE) init-proxy


update-domain:
	@# domain: domain url
	@sed -i 's/DOMAIN/$(domain)/g' proxy/makefile
	@sed -i 's/DOMAIN/$(domain)/g' proxy/data/nginx/conf/nginx.conf
	@sed -i 's/DOMAIN/$(domain)/g' proxy/data/nginx/site_template.conf
	@sed -i 's/DOMAIN/$(domain)/g' proxy/init/init
	@sed -i 's/DOMAIN/$(domain)/g' proxy/init/nginx/nginx.conf


############
# new site #
############

restart-proxy:
	@cd proxy && $(MAKE) restart


add-site:
	@# site: site subdomain (without domain part)
	@# port: site port
	@# db_port: site db port
	@cd proxy && $(MAKE) add-site site=$(site) port=$(port)
	@cd db && $(MAKE) up site=$(site) port=$(db_port)


##########
# server #
##########

update-server:
	@ # token: gitlab token
	@ # image: image to use
	@ # site: site subdomain (without domain part)
	@mkdir -p $(site)-logs
	@sudo podman login -u deploy -p $(token) registry.gitlab.com
	@sudo podman stop $(site)-server || true
	@sudo podman rm $(site)-server || true
	@sudo podman run \
		-dt \
		--pod pod \
		--name $(site)-server \
		--env-file=.env \
		-v ./$(site)-logs:/app/logs \
		$(image)


migrate-db:
	@ # token: gitlab token
	@ # image: image to use
	@ # site: site subdomain (without domain part)
	@sudo podman login -u deploy -p $(token) registry.gitlab.com
	@sudo podman run \
		-dt \
		--pod pod \
		--name $(site)-migrate \
		--env-file=.env \
		--rm \
		$(image) \
		aerich upgrade
