server {
  listen 443 ssl;
  server_name SITE.DOMAIN;

  ssl_certificate /etc/letsencrypt/live/SITE.DOMAIN/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/SITE.DOMAIN/privkey.pem;
  include /etc/letsencrypt/options-ssl-nginx.conf;
  ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

  gzip on;
  gzip_vary on;
  gzip_proxied any;
  gzip_types text/plain text/css text/javascript application/json image/svg+xml;

  location / {
    proxy_pass http://127.0.0.1:PORT;
    add_header Cache-Control 'no-cache';
    add_header Strict-Transport-Security 'max-age=31536000; includeSubDomains; preload';
    add_header X-XSS-Protection "1; mode=block";
    add_header Content-Security-Policy "default-src 'self'";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Frame-Options "DENY";
    add_header X-Content-Type-Options "nosniff";
    add_header Referrer-Policy "no-referrer";
    add_header Feature-Policy "geolocation 'none'; midi 'none'; notifications 'none'; push 'none'; sync-xhr 'none'; microphone 'none'; camera 'none'; magnetometer 'none'; gyroscope 'none'; speaker 'none'; vibrate 'none'; fullscreen 'none'; payment 'none'";
    add_header Accept-Encoding "br";
  }
}

server {
  listen 80;
  server_name SITE.DOMAIN;

  location /.well-known/acme-challenge/ {
    allow all;
    root /var/www/certbot;
  }

  location / {
    return 301 https://$host$request_uri;
  }
}
