#!/bin/sh

sudo podman run \
  -dt \
  --pod pod \
  -v ./data/certbot/conf:/etc/letsencrypt \
  -v ./data/certbot/www:/var/www/certbot \
  --name certbot \
  certbot/certbot \
  renew
