import sys
from http.server import BaseHTTPRequestHandler, HTTPServer


class Handler(BaseHTTPRequestHandler):
    def do_GET(self: "Handler") -> None:
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b"Pong!")


if __name__ == "__main__":
    if len(sys.argv) == 2:
        port = int(sys.argv[1])
    else:
        port = 8000

    httpd = HTTPServer(("0.0.0.0", port), Handler)
    print(f"Server running on port {port}")
    httpd.serve_forever()
